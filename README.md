# code-challenge

Hi and welcome to **Mavtek**'s Code Challenge. Please take your time to read the requirements carefully. You have one week to submit an answer in form of a Pull Request.

# Problem

You are given the task of building a mechanism to upload, store and serve images in the most efficient way possible. 
Your solution should be able to sustain the load of a high traffic website.

Build a form that allows a user to upload an image and responds with two links:

- Link to the original image
- Link to a resized version of the original image with the following dimensions: 128*128

## Requirements

- Only accept images of type jpg, png and with a maximum file size of 10mb
- Designing for cloud and/or using containers is a bonus

## Questions

- Explain your approach using a tool of your choice: text, diagrams, drawing... 
- Implement your solution using your frameworks, packages, tools of choice.
- Explain what more could you have done, given more time and resources, to take your current solution to the next level.
